#!/bin/bash

read -p "Username (empty for current user): " username

if test -z "$username"
then
	username="$(whoami)"
fi

while true; do
	read -s -p "Password: " password
	[ ! -z "$password" ] && break
	echo
	echo "Password cant be empty, try again"
done

echo

[ "$(echo -n "$password" | md5sum)" = "$(head -2 login | tail -1)" ] && [ "$username" = "$(head -1 login | tail -1)" ] && echo "Login successful!"

