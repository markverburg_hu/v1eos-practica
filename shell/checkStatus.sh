#!/bin/bash

rm -f "$3"

find "$1"/* | while read file; do
    if "$2" "$file" > /dev/null 2>&1; then
        echo $(basename "$file") success! >> "$3"
    else
	echo $(basename "$file") fail! >> "$3"
    fi
done

