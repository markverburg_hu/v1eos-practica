#!/bin/bash

find "$1" -iname \*.jpg | while read file; do
    echo Converting $(basename "$file")
    convert "$file" -resize 128x128 "${file%.jpg}.png"
done

