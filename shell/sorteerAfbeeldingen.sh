#!/bin/bash

mkdir -p Afbeeldingen

find "$1" -iname \*.png -o -iname \*.jpg | while read file; do
    echo Copying $(basename "$file")
    cp "$file" Afbeeldingen/$(basename "$file")
done

