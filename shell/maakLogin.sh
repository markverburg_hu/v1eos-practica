#!/bin/bash

read -p "Enter username (empty for current user): " username

if test -z "$username"
then
	username="$(whoami)"
fi

while true; do
	read -s -p "Enter password: " password
	echo
	read -s -p "Confirm password: " password2
	echo
	[ "$password" = "$password2" ] && [ ! -z "$password" ] && break
	echo "Passwords do not match"
done

echo "$username" > login
echo -n "$password" | md5sum >> login
echo "Password saved!"

