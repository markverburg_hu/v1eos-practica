#include "shell.hh"

int main() {
    std::string input;
    char prompt[1024];

    size_t fd = syscall(SYS_open, "prompt", O_RDONLY);
    prompt[syscall(SYS_read, fd, &prompt, 1024)-1] = '\0';
    syscall(SYS_close, fd);

    while(true) {

        std::cout << prompt;
        std::getline(std::cin, input);

        if (input == "new_file") new_file();
        else if (input == "ls") list();
        else if (input == "src") src();
        else if (input == "find") find();
        else if (input == "seek") seek();
        else if (input == "exit") return 0;
        else if (input == "quit") return 0;
        else if (input == "error") return 1;
        if (std::cin.eof()) return 0;
    }
}

void new_file() {
    std::string filename;
    std::cout << "Enter filename: ";
    std::getline(std::cin, filename);

    size_t fd = syscall(SYS_open, filename.c_str(), O_RDONLY | O_CREAT, 00600);

    std::string line;
    while(line != "<EOF>") {
        std::getline(std::cin, line);
        if(line != "<EOF>") syscall(SYS_write, fd, std::string(line + "\n").c_str(), line.size() + 1);
    }
    syscall(SYS_close, fd, 0, 0);
}

void list() {
    pid_t pid = syscall(SYS_fork);

    if(pid == 0) {
        const char* args[] = { "/bin/ls", "-la", NULL };
        syscall(SYS_execve, "/bin/ls", args, nullptr);
    }
    else if(pid > 0) {
        int call = syscall(SYS_waitid, P_PID, pid, nullptr, WEXITED);
    }
}

void find() {
    int fd[2];
    syscall(SYS_pipe, fd);
    pid_t pid = syscall(SYS_fork);

    if(pid == 0) {
        syscall(SYS_dup2, fd[1], STDOUT_FILENO);

        syscall(SYS_close, fd[0]);
        syscall(SYS_close, fd[1]);

        const char* args[] = { "/bin/find", ".", NULL };
        syscall(SYS_execve, "/bin/find", args, nullptr);
    }
    else if(pid > 0) {
        pid = syscall(SYS_fork);

        if(pid == 0) {
            syscall(SYS_dup2, fd[0], STDIN_FILENO);

            syscall(SYS_close, fd[0]);
            syscall(SYS_close, fd[1]);

            const char* args[] = { "/bin/grep", "string", NULL };
            syscall(SYS_execve, "/bin/grep", args, nullptr);
        }
        else if(pid > 0) {
            syscall(SYS_close, fd[0]);
            syscall(SYS_close, fd[1]);

            syscall(SYS_waitid, P_PID, pid, nullptr, WEXITED);
        }
    }
}

void seek() {
    const char buffer[1] = { 'x' };
    const char empty[1] = { '\0' };

    size_t seek = syscall(SYS_open, "seek", O_WRONLY | O_CREAT, 00600);
    syscall(SYS_write, seek, &buffer, 1);
    syscall(SYS_lseek, seek, 5000000, SEEK_CUR);
    syscall(SYS_write, seek, &buffer, 1);
    syscall(SYS_close, seek);

    size_t loop = syscall(SYS_open, "loop", O_WRONLY | O_CREAT, 00600);
    syscall(SYS_write, loop, &buffer, 1);
    for(unsigned int i = 0; i < 5000000; i++) {
        syscall(SYS_write, loop, &empty, 1);
    }
    syscall(SYS_write, loop, &buffer, 1);
    syscall(SYS_close, loop);
}

void src() {
    int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755);
    char byte[1];

    while(syscall(SYS_read, fd, byte, 1)) {
        std::cout << byte;
    }
}
