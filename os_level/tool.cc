#define uint64_t unsigned long long

#define SYS_read	0
#define	SYS_write	1

#define STDIN_FILENO	0
#define STDOUT_FILENO	1
#define STDERR_FILENO	2

uint64_t syscall(uint64_t syscall = 0, uint64_t arg1 = 0, uint64_t arg2 = 0, uint64_t arg3 = 0) {
    uint64_t retvalue = 0;
    __asm__(
        "mov	%[syscall], %%rax;"
        "mov	%[arg1], %%rdi;"
        "mov	%[arg2], %%rsi;"
        "mov	%[arg3], %%rdx;"
        "syscall;"
        "mov    %%rax, %[retvalue];"
        :[retvalue]"=g"(retvalue)
        :[syscall]"g"(syscall), [arg1]"g"(arg1), [arg2]"g"(arg2), [arg3]"g"(arg3)
        :"%rdx", "%rsi", "%rdi"
    );
	return retvalue;
}

int strlen(const char *str) {
	int length;
	for (length = 0; str[length] != '\0'; ++length);
	return length;
}

uint64_t print(const char *str, int output) {
	return syscall(SYS_write, output, (uint64_t)str, strlen(str));
}

const char* read_line() {
	char* buffer = new char[1024];
	buffer[syscall(SYS_read, STDIN_FILENO, (uint64_t)buffer, 1024)-1] = '\0';
	return buffer;
}

class string {
	public:
		string(const char* str) {
			int length = strlen(str);
			this->str = new char[length];

			for(int i = 0; i < length; i++) {
				this->str[i] = str[i];
			}
		}
		string reversed() {
			int length = strlen(this->str);
			char *buffer = new char[length];

			for(int i = 0; i < length; i++) {
				buffer[i] = this->str[length - i - 1];
			}

			string str = buffer;
			delete[] buffer;
			return str;
		}
		string operator = (const char* str) {
			int length = strlen(str);
			this->str = new char[length];

			for(int i = 0; i < length; i++) this->str[i] = str[i];

			return *this;
		}
		void operator += (const char* str) {
			int s1 = strlen(this->str);
			int s2 = strlen(str);

			char* buffer = new char[s1+s2];

			for(int i = 0; i < s1; i++) buffer[i] = this->str[i];
			for(int i = 0; i < s2; i++) buffer[s1+i] = str[i];

			delete[] this->str;
			this->str = buffer;
		}
		string operator + (const char* str) {
			int s1 = strlen(this->str);
			int s2 = strlen(str);

			char* buffer = new char[s1+s2];

			for(int i = 0; i < s1; i++) buffer[i] = this->str[i];
			for(int i = 0; i < s2; i++) buffer[s1+i] = str[i];

			return string(buffer);
		}
		const char* c_str() {
			return this->str;
		}
		~string() {
			delete[] this->str;
		}
	private:
		char* str;
};

uint64_t print(string str, int output) {
	return print(str.c_str(), output);
}

bool strcmp(const char* lhs, const char* rhs) {
	if(strlen(lhs) == strlen(rhs)) {
		for (int i = 0; i < strlen(lhs); i++) {
			if(lhs[i] != rhs[i]) {
				return false;
			}
		}
		return true;
	}
	return false;
}

int main (int argc, char *argv[]) {
	if(argc == 2 && strcmp(argv[1], "-reverse")) {
		print(string(read_line()).reversed() + "\n", STDOUT_FILENO);
	}
	else {
		print("Usage: -reverse\n", STDERR_FILENO);
	}
	return 0;
}
